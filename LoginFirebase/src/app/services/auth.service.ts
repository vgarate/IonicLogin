import { Injectable } from '@angular/core';

import { Router } from "@angular/router";

import { AngularFireAuth } from "@angular/fire/auth";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private AFauth: AngularFireAuth, private router: Router) {

  }

  login(email: string, pass: string){

    return new Promise((resolve, rejected) => {
      this.AFauth.auth.signInWithEmailAndPassword(email, pass).then( user => {
        resolve(user);
      }).catch( err => {
        rejected(err);
      });
    });
  }

  logout(){
    this.AFauth.auth.signOut().then( () => {
      this.router.navigate(['/login'])
    })
  }
}
